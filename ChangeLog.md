# ChangeLog

## 2.0.0 - 2016-05-15

### Added

* Everything - first modern Perl version

## 2.0.1 - 2016-05-15

### Added

* README.md
* ChangeLog.md
* MANIFEST

